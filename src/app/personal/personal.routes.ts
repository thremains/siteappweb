/**
 * Created by think on 2017/6/20.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PersonalComponent} from './personal.component';
const userRoutes: Routes = [
  {
    path: '',
    component: PersonalComponent,
    children: [
      {
        path: '',
        component: PersonalComponent,
      },
      {
        path: 'user',
        component: PersonalComponent,
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PersonlRoutingModule {
}
