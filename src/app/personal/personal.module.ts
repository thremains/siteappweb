import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonalComponent } from './personal.component';
import { PersonlRoutingModule } from './personal.routes';

@NgModule({
  imports: [
    CommonModule,
    PersonlRoutingModule
  ],
  declarations: [PersonalComponent]
})
export class PersonalModule { }
