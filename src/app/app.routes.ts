import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ExceptionComponent} from './exception/exception.component';
const appRoutes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'user', loadChildren: './user/user.module#UserModule'},
  {path: 'home', loadChildren: './home/home.module#HomeModule'},
  {path: 'personal', loadChildren: './personal/personal.module#PersonalModule'},
  {path: 'team', loadChildren: './team/team.module#TeamModule'},
  {path: 'pursuit', loadChildren: './pursuit/pursuit.module#PursuitModule'},
  {path: 'timeline', loadChildren: './timeline/timeline.module#TimelineModule'},
  {path: 'type', loadChildren: './type/type.module#TypeModule'},
  {path: '**', component: ExceptionComponent}
];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
