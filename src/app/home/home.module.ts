import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {HomeRoutingModule} from './home.routes';
import {ClassificationComponent} from './classification/classification.component';
import {ContentComponent} from './content/content.component';
import {TimeComponent} from './time/time.component';


@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule
  ],
  declarations: [HomeComponent, ClassificationComponent, ContentComponent, TimeComponent],
})

export class HomeModule {
}
