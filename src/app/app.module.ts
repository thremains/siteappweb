import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AlertModule} from 'ngx-bootstrap';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app.routes';
import {ExceptionComponent} from './exception/exception.component';

@NgModule({
  declarations: [
    AppComponent,
    ExceptionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AlertModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
