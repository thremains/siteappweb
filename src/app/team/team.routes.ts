/**
 * Created by think on 2017/6/20.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TeamComponent} from './team.component';
const userRoutes: Routes = [
  {
    path: '',
    component: TeamComponent,
    children: [
      {
        path: '',
        component: TeamComponent,
      },
      {
        path: 'user',
        component: TeamComponent,
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TeamRoutingModule {
}
