/**
 * Created by think on 2017/6/20.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TypeComponent} from './type.component';
const userRoutes: Routes = [
  {
    path: '',
    component: TypeComponent,
    children: [
      {
        path: 'user',
        component: TypeComponent,
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TypeRoutingModule {
}
