/**
 * Created by think on 2017/6/20.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TimelineComponent} from './timeline.component';
const userRoutes: Routes = [
  {
    path: '',
    component: TimelineComponent,
    children: [
      {
        path: '',
        component: TimelineComponent,
      },
      {
        path: 'user',
        component: TimelineComponent,
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TimelineRoutingModule {
}
