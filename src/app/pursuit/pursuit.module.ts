import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PursuitComponent} from './pursuit.component';
import {PursuitRoutingModule} from './pursuit.routes';

@NgModule({
  imports: [
    CommonModule,
    PursuitRoutingModule
  ],
  declarations: [PursuitComponent]
})
export class PursuitModule {
}
