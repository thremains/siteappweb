/**
 * Created by think on 2017/6/20.
 */
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PursuitComponent} from './pursuit.component';
const userRoutes: Routes = [
  {
    path: '',
    component: PursuitComponent,
    children: [
      {
        path: '',
        component: PursuitComponent,
      },
      {
        path: 'user',
        component: PursuitComponent,
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(userRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PursuitRoutingModule {
}
