import { SiteappwebPage } from './app.po';

describe('siteappweb App', () => {
  let page: SiteappwebPage;

  beforeEach(() => {
    page = new SiteappwebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
